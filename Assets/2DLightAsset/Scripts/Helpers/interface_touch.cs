﻿namespace DynamicLight2D
{
	using UnityEngine;
	using System.Collections;
	
	public class interface_touch: MonoBehaviour {
        public GameObject character;
		GameObject cLight;
		GameObject cubeL;
		
		//GUIText UIlights;
		//GUIText UIvertex;
		
		
		[HideInInspector] public static int vertexCount;
		
		//int lightCount = 1;
		
		
		void Start () {
			cLight = GameObject.Find("2DLight");
			
			StartCoroutine (LoopUpdate ());
			
		}
		
		// Update is called once per frame
		IEnumerator LoopUpdate () {
			
			while (true) {
				Vector3 pos = character.transform.position;
			
				yield return new WaitForEndOfFrame ();
				cLight.transform.position = pos;
				
			}
			
			
		}
		
		
		
	}

}