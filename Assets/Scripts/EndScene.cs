using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class EndScene : MonoBehaviour
{
    #region MONO BEHAAVIOURS
    // Start is called before the first frame update
    void Start()
    {
        DialogeSystem.Instance.EndText();
        Invoke(nameof(BackToMenu), 30);
    }
    #endregion

    #region PRIVATE METHODS
    void BackToMenu()
    {
        SceneManager.LoadScene(nameof(Start));
    }
    #endregion
}
