using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class CharacterMovement : MonoBehaviour
{
    #region SERIALIZED FIELDS
    [SerializeField]
    private Color defaultColor;
    [SerializeField]
    private Material lightMat;
    [SerializeField]
    private Rigidbody2D rb;
    #endregion

    #region PRIVATE FIELDS
    private Vector2 normalGravity;
    bool doubleJump;
    bool grounded;
    #endregion

    #region MONO BEHAVIOURS
    private void Start()
    {
        lightMat.color = defaultColor;
        LevelManager.Instance.SetLevel(LevelManager.LevelType.level1);
        Invoke(nameof(LoadDialog), 2);
    }
    void Update()
    {
        Vector3 pos = transform.position;
        pos.x += Input.GetAxis("Horizontal") * 10f * Time.deltaTime;
        transform.position = pos;
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (grounded || !doubleJump)
            {
                if (doubleJump)
                {
                    SoundPlayer.Instance.PlaySound(SoundPlayer.SoundClip.Jump5);
                    doubleJump = false;
                    rb.AddForce((LevelManager.Instance.GetLevel() == LevelManager.LevelType.level2 ? Vector3.down : Vector3.up) * 8, ForceMode2D.Impulse);
                }
                else
                {
                    int rand = Random.Range(0, 5);
                    SoundPlayer.Instance.PlaySound((SoundPlayer.SoundClip)rand);
                    doubleJump = true;
                    rb.AddForce((LevelManager.Instance.GetLevel() == LevelManager.LevelType.level2 ? Vector3.down : Vector3.up) * 12, ForceMode2D.Impulse);
                }
                grounded = false;

            }
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            for (int i = 1; i < transform.childCount; i++)
            {

                transform.GetChild(i).parent = null;
                transform.position = transform.position + Vector3.up * 5;
            }
        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "ground")
        {
            grounded = true;
            transform.rotation = Quaternion.identity;
        }
        else if (collision.gameObject.tag == "encounter")
        {
            transform.position = LevelManager.Instance.GetLevelBeginPoint();
        }
        if (collision.gameObject.tag == "end")
        {
            SoundPlayer.Instance.PlaySound(SoundPlayer.SoundClip.win);
            Invoke(nameof(EndScene), 3);
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "open")
        {
            LevelManager.Instance.SetLight(lightMat);
            LevelManager.Instance.GetLevelStructure().opened = true;
            LevelManager.Instance.GetLevelStructure().closePortal.isTrigger = true;
        }
        if (collision.gameObject.tag == "closeEnter")
        {
            lightMat.color = defaultColor;
            LevelManager.Instance.GetLevelStructure().closePortal.isTrigger = false;
            LevelManager.Instance.SetLevel(LevelManager.Instance.GetLevel() + 1);
            transform.position = LevelManager.Instance.GetLevelBeginPoint();
            Invoke(nameof(LoadDialog), 5);
            if (LevelManager.Instance.GetLevel() == LevelManager.LevelType.level2)
            {
                normalGravity = Physics2D.gravity;
                Physics2D.gravity = -Physics2D.gravity;
            }
            else
                Physics2D.gravity = normalGravity;
        }
        if (collision.gameObject.tag == "end")
        {
            SoundPlayer.Instance.PlaySound(SoundPlayer.SoundClip.win);
            Invoke(nameof(EndScene), 3);
        }
    }
    #endregion

    #region PRIVATE METHODS
    private void EndScene()
    {
        SceneManager.LoadScene("3DEnd");
    }
    private void LoadDialog()
    {
        if (LevelManager.Instance.GetLevel() == LevelManager.LevelType.level1)
            DialogeSystem.Instance.Level1Text();
        else if (LevelManager.Instance.GetLevel() == LevelManager.LevelType.level2)
            DialogeSystem.Instance.Level2Text();
        else if (LevelManager.Instance.GetLevel() == LevelManager.LevelType.level3)
            DialogeSystem.Instance.Level3Text();
        else if (LevelManager.Instance.GetLevel() == LevelManager.LevelType.level4)
            DialogeSystem.Instance.Level4Text();
    }
    #endregion

}
