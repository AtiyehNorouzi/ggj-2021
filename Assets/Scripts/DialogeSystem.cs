﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Doublsb.Dialog;

/// <summary>
/// A script i wrote with help of the dialoge base system that i downloaded from asset store
///https://assetstore.unity.com/packages/tools/gui/d-dialogue-system-167312
/// </summary>
public class DialogeSystem : MonoBehaviour
{
    #region PUBLIC FIELDS
    public DialogManager DialogManager;
    #endregion

    #region STATIC FIELDS
    private static DialogeSystem _instance;
    #endregion

    #region PROPERTIES
    public static DialogeSystem Instance
    {
        get
        {
            if (!_instance)
                _instance = FindObjectOfType<DialogeSystem>();
            return _instance;
        }

    }
    #endregion

    #region PUBLIC METHODS
    public void Level1Text()
    {
        var dialogTexts = new List<DialogData>();

        dialogTexts.Add(new DialogData("Caitlin was lost in her head and she needs to find a way out./close/", "Chris"));

        dialogTexts.Add(new DialogData("she was feeling isolated. she couldn't find the connection between her and the world anymore. The space in between was /speed:down/ SCARY! /speed:0.1/ /close/", "Chris"));

        dialogTexts.Add(new DialogData("but caitlin knew that there must be a /color:blue/ WAY /color:white/ through it. /close/", "Chris"));

        DialogManager.Show(dialogTexts);
    }

    public void Level2Text()
    {
        var dialogTexts = new List<DialogData>();

        dialogTexts.Add(new DialogData("Caitlin was thinking a lot about the past and future /close/", "Chris"));

        dialogTexts.Add(new DialogData("what is going to happen? what did i do wrong?... what if it isn't my /speed:down/ FAULT! /speed:0.1/  /close/", "Chris"));

        DialogManager.Show(dialogTexts);
    }
    public void Level3Text()
    {
        var dialogTexts = new List<DialogData>();

        dialogTexts.Add(new DialogData("She felt lonely. she didn't remember what her friends looked like. Their IMAGE /close/", "Chris"));

        dialogTexts.Add(new DialogData("became blurry and distant. But surely, my friends are still there.  somewhere!  /close/", "Chris"));

        DialogManager.Show(dialogTexts);
    }
    public void Level4Text()
    {
        var dialogTexts = new List<DialogData>();

        dialogTexts.Add(new DialogData("Caitlin kept wondering what was happening with the world. she didn't understand it right now. /close/", "Chris"));

        dialogTexts.Add(new DialogData("She knew things were not right. but nobody was saying it. she wanted to scream!/close/", "Chris"));

        dialogTexts.Add(new DialogData("She said it all out /color:blue/ LOUD /color:white/ /close/", "Chris"));

        DialogManager.Show(dialogTexts);
    }
    public void EndText()
    {
        var dialogTexts = new List<DialogData>();

        dialogTexts.Add(new DialogData("Suddenly,things began to take shape. everything started to become a little bit lighter, sharper and more vivid. /close/", "Chris"));

        dialogTexts.Add(new DialogData("There was still a long way to go but she could see a way out. she didn't feel great, but that was OK! /close/", "Chris"));

        dialogTexts.Add(new DialogData(" She could feel her friend's presence and she could sense /color:blue/ good /color:white/ things coming her way /close/", "Chris"));

        DialogManager.Show(dialogTexts);
    }
    #endregion
}
