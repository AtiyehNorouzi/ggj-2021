using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EdgeForce : MonoBehaviour
{
    #region SERIALIZED FIELDS
    [SerializeField]
    private dirForce forceDir;
    #endregion

    #region ENUMS
    public enum dirForce
    {
        up, down, left, right
    }
    #endregion

    #region MONO BEHAVIOURS
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "character")
        {
            collision.gameObject.GetComponent<Rigidbody2D>().AddForce(GetVector(forceDir) * 5, ForceMode2D.Impulse);
        }
    }
    #endregion

    #region PRIVATE METHODS
    private Vector3 GetVector(dirForce forceDir)
    {
        if (forceDir == dirForce.up)
            return Vector3.up;
        else if (forceDir == dirForce.down)
            return Vector3.down;
        else if (forceDir == dirForce.right)
            return Vector3.right;
        return Vector3.left;
    }
    #endregion
}
