using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    #region STATIC FIELDS
    private static LevelManager _instance;
    #endregion

    #region PRIVATE FIELDS
    private int currentLevel = 0;
    #endregion

    #region PROPERTIES
    public static LevelManager Instance => (_instance == null) ? GameObject.FindObjectOfType<LevelManager>() : _instance;
    #endregion

    #region ENUMS
    public enum LevelType
    {
        defaultLevel = 0,
        level1,
        level2,
        level3,
        level4,
        level5
    }
    #endregion

    #region SERIALIZED FIELDS
    [SerializeField]
    List<LevelStructure> levels;
    #endregion

    #region STRUCTS
    [System.Serializable]
    public class LevelStructure
    {
        public LevelType level;
        public Transform beginPoint;
        public Color lightColor;
        public Collider2D openPortal;
        public Collider2D closePortal;
        public bool opened;
    }
    #endregion

    #region PUBLIC METHODS
    public Vector3 GetLevelBeginPoint()
    {
        return levels[currentLevel].beginPoint.position;
    }
    public void SetLight(Material mat)
    {
        mat.color = levels[currentLevel].lightColor;
    }
    public void SetLevel(LevelType type)
    {
        currentLevel = (int)type;
    }
    public LevelType GetLevel()
    {
        return (LevelType)currentLevel;
    }
    public LevelStructure GetLevelStructure()
    {
        return levels[currentLevel];
    }
    #endregion

}
