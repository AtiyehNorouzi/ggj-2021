using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickableObject : MonoBehaviour
{
    #region MONO BEHAVIOURS
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "character")
        {
            if(collision.gameObject.transform.childCount == 1)
                gameObject.transform.parent.parent = collision.gameObject.transform;
        }
    }
    #endregion
}
